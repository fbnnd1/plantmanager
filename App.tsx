import React from 'react';
//import React, {useEffect} from 'react';
import AppLoading from 'expo-app-loading';
//import * as Notifications from 'expo-notifications';

import Routes from './src/routes';

import { Jost_400Regular, Jost_600SemiBold, useFonts } from '@expo-google-fonts/jost';
//import { PlantProps } from './src/libs/storage';

export default function App() {
  const [fontsLoaded] = useFonts({Jost_400Regular, Jost_600SemiBold});
/*
  useEffect(() => {
    
    const subscription = Notifications.addNotificationReceivedListener(
      async notification => {
        const data = notification.request.content.data.plant as PlantProps
      }
    );
      
    async function notifications() {
      const data = await Notifications.getAllScheduledNotificationsAsync();
      console.log("######## Notificações agendadas ###########");
      console.log(data);

      await Notifications.cancelAllScheduledNotificationsAsync();
    }
    
    //notifications();
  }, []);
*/

  if (!fontsLoaded) {
    return <AppLoading />
  }
  
  return (
    <Routes />
  );
}

import React from 'react';
import {  Text, StyleSheet, TouchableOpacity, TouchableOpacityProps} from 'react-native';

import colors from '../styles/colors';
import fonts from  '../styles/fonts';

interface ButtonProps extends TouchableOpacityProps {
    title: String;
}

export function Button({title, ...rest }:ButtonProps) {
    return (
        <TouchableOpacity style={style.container} activeOpacity={0.7} {...rest}>
            <Text style={style.text}>
               {title}
            </Text>
        </TouchableOpacity>
    );
}

const style = StyleSheet.create({

    container: {
        backgroundColor: colors.green,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 16,//pixels
        marginBottom: 10,
        height: 56,
        //width: 56,
    },

    text: {
        color: colors.white,
        fontSize: 16,
        textAlign: 'center',
        fontFamily: fonts.heading,
    }
  });

import React, {useEffect, useState} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
} from 'react-native';

import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import AsyncStorage from '@react-native-async-storage/async-storage';

import colors from '../styles/colors';
import fonts from '../styles/fonts';
import userImg from '../assets/watering.png';

export function Header() {

    const [userName, setUserName] = useState<string>();

    useEffect(() => {
        async function loadSotrageUserName() {
            const user = await AsyncStorage.getItem("@plantmanager:user");
            setUserName(user || '');
        }

        loadSotrageUserName();
    }, []);

    return (
        <View style={styles.container}>
            <Text style={styles.greeting}>Olá</Text>
            <Text style={styles.userName}>{userName}</Text>
            <Image 
            source={userImg} 
            style={styles.image} 
            resizeMode="contain"
          />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width:'100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: getStatusBarHeight(),
        paddingVertical:20,
        backgroundColor: colors.background,
    },
    greeting: {
        fontSize: 32,
        color: colors.heading,
        fontFamily: fonts.heading,
    },
    userName: {
        fontSize: 32,
        color: colors.heading,
        fontFamily: fonts.heading,
        lineHeight: 40,
    },
    image: {
        width: 80,
        height: 80,
        borderRadius: 40,
    }
});
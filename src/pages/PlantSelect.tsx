import React, {useEffect, useState} from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    ActivityIndicator,
} from 'react-native';

import { useNavigation } from '@react-navigation/core';

import colors from '../styles/colors';
import fonts from '../styles/fonts';

import { Header } from '../components/Header';
import { EnviromentButton } from '../components/EnviromentButton';
import api from '../services/api';
import { PlantCardPrimary } from '../components/PlantCardPrimary';
import {Load} from '../components/Load';
import { PlantProps } from '../libs/storage';


interface EnviromentProps {
    key: string;
    title: string;
}

export function PlantSelect() {

    const navigation = useNavigation();

    const [enviroments, setEnviroments] = useState<EnviromentProps[]>([]);
    const [plants, setPlants] = useState<PlantProps[]>([]);
    const [enviromentSelected, setEnviromentSelected] = useState('all');
    const [filteredPlants, setFilteredPlants] = useState<PlantProps[]>([]);
    const [loading, setLoading] = useState(true);
    const [loadingMore, setLoadingMore] = useState(true);
    const [page, setPage] = useState(1);

    function handleEnviromentSelected(enviroment:string) {
        setEnviromentSelected(enviroment);

        if (enviroment == 'all')
            return setFilteredPlants(plants);

        const filtered = plants.filter(plant => 
            plant.environments.includes(enviroment)
        );

        setFilteredPlants(filtered);
    }

    async function fetchPlants() {
        const { data } = await api
            .get(`plants?_sort=name&_order=asc&_page=${page}&_limit=8`);
        
        if (!data) {
            setLoading(true);
        }
        
        if (page > 1) {
            setPlants(oldValue => [...oldValue, ...data]);
            setFilteredPlants(oldValue => [...oldValue, ...data]);
        } else {
            setPlants(data);
            setFilteredPlants(data);
        }

        setLoading(false);
        setLoadingMore(false);

    }

    useEffect(() => {
        async function fetchEnviroment() {
            const data = await api.get('plants_environments?_sort=title&_order=asc');
            setEnviroments([{
                key:'all',
                title:'Todos',
             ...data} ]);

        }

        fetchEnviroment();
    }, []);

    /*
    useEffect(() => {
        async function fetchPlants() {
            const  {data} = await api.get('plants?_sort=name&_order=asc');
            setPlants(data);
            setFilteredPlants(data);
            setLoading(false);
        }

        fetchPlants();
    }, []);
    */

    useEffect(() => {
        fetchPlants();
    }, []);

    function handleFetchMore(distance: number) {
        if (distance < 1) {
            return;
        }

        setLoadingMore(true);
        setPage(oldValue => oldValue + 1);
        fetchPlants();
    }

    function handlePlantSelect(plant: PlantProps) {
        navigation.navigate("PlantSelect");
    }

    if (loading) {
        return <Load />
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Header />
                <Text style={styles.title}>Em qual ambiente</Text>
                <Text style={styles.subtitle}>você quer colocar a planta?</Text>
            </View>

            <View>
                <FlatList data={enviroments} 
                    keyExtractor={(item) => item.key}
                    renderItem={({item}) => (
                        <EnviromentButton 
                            title={item.title} 
                            active={item.key === enviromentSelected} 
                            onPress={() => handleEnviromentSelected(item.key)}
                        />
                    )}
                
                horizontal
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={styles.envorimentList}
                
                />
            </View>

            <View style={styles.plants}>
                <FlatList 
                    data={filteredPlants}
                    keyExtractor={(item) => item.id}
                    renderItem={( { item }) => (
                        <PlantCardPrimary data={item} onPress={() => handlePlantSelect(item)} />
                    )}
                    showsVerticalScrollIndicator={false}
                    numColumns={2}
                    onEndReachedThreshold={0.1}
                    onEndReached={({ distanceFromEnd }) => 
                        handleFetchMore(distanceFromEnd)
                    }
                    ListFooterComponent={
                        loadingMore ? <ActivityIndicator color={colors.green} />: <></>
                    }
                    /*contentContainerStyle={styles.contentContainerStyle}*/
                />
            </View>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.background,
       
    }, 
    header: {
        paddingHorizontal: 30,
    },
    title: {
        fontSize: 17,
        color: colors.heading,
        fontFamily: fonts.heading,
        lineHeight: 20,
        marginTop: 15,
    },
    subtitle: {
        fontSize: 17,
        color: colors.heading,
        fontFamily: fonts.text,
        lineHeight: 20,
    },
    envorimentList: {
        height: 40, 
        justifyContent: 'center',
        marginBottom: 5,
        marginVertical:  32,
        marginLeft:32,
    },
    plants: {
        flex: 1,
        paddingHorizontal: 32,
        justifyContent: 'center'
    }
});
import React from 'react';
import {
  SafeAreaView,
    Text, 
    View, 
    StyleSheet, 
    Image, 
    Dimensions, 
    TouchableOpacity
} from 'react-native';

import { Feather } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/core';

import wateringImg from '../assets/watering.png';
import colors from '../styles/colors';
import fonts from '../styles/fonts';

export function Welcome() {
  const navigation = useNavigation();

  function handleStart() {
    navigation.navigate('UserIdentification');
  }

    return (
      <SafeAreaView style={styles.contaiener}>
        <View style={styles.wrapper} >

          <Text style={styles.title}>
            Gerencie {'\n'} suas plantas de {'\n'} forma fácil.
          </Text>

          <Image 
            source={wateringImg} 
            style={styles.image} 
            resizeMode="contain"
          />

          <Text style={styles.subtitle}>
              Não esqueça de regar suas plantas.
              Nós cuidamos de lembrar você sempre que precisar.
          </Text>

          <TouchableOpacity 
            style={styles.button} 
            activeOpacity={0.7}
            onPress={handleStart}
          >
              <Text style={styles.buttonIcon}>
                <Feather name="chevron-right" />
              </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }

  const styles = StyleSheet.create({
    contaiener: {
      flex: 1,
    },
    wrapper: {
      flex: 1,
      justifyContent: 'space-around',
      alignItems: 'center',
      paddingHorizontal: 20,
    },
    title: {
        fontSize: 28,
        //fontWeight: 'bold',
        textAlign: 'center',
        color: colors.heading,
        marginTop: 38,
        fontFamily: fonts.heading,
        lineHeight: 34,
    },
    subtitle: {
        textAlign: 'center',
        paddingHorizontal: 20,
        fontSize: 18,
        color: colors.heading,
        fontFamily: fonts.text,
    },
    
    image : {
        height: Dimensions.get('window').width  * 0.7,

    },

    button: {
      backgroundColor: colors.green,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 16,//pixels
      marginBottom: 10,
      height: 56,
      width: 56,
  },

  buttonIcon: {
      color: colors.white,
      fontSize: 32,
      //textAlign: 'center',
  }

  });